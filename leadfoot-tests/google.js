'use strict';

//setup vars for testing
var assert = require('chai').assert;

//the actual domain to be tested
var domain = 'http://www.google.co.th/?hl=en',
	command = kommando.command;

/*
//	if you need to do something before the suites run use this...
*/
// suiteSetup(function() {
// 	console.log('--------------SUITE SETUP-----------------');
// });

suite('leadfoot', function(){
	test('check feeling lucky button', function(done){
		command
		.get(domain)
		.findByCssSelector('input[name="btnI"]')
		.isDisplayed()
		.then(function(result){
			return assert.equal(result, true, 'check that content element exists');
		})
		.then(done, done);
	});
});
