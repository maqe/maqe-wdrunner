# MAQE wdrunner - Webdriver Test Runner

Webdriver test runner - both with local selenium as well as browserstack support.

Currently heavily based on ```kommando``` project with some modifications.

The runner currently only supports:

- ```mocha``` testing framework
- ```chai``` assertion library
- ```wd``` webdriver client
- ```leadfoor``` webdriver client

Eventually look to add support for:

- other testing frameworks and assertion library

---

Project can now be installed globally and available via command line as ```maqewdrunner```.

```
npm install -g git+ssh://git@bitbucket.org:maqe/maqe-wdrunner.git
```

Note: typically will need to install via ```sudo```. If the project is private and your deployment key is tied to a non-sudo user you may need to follow the steps in this comment: https://github.com/npm/npm/issues/10730#issuecomment-162943001

---

If you want to run against a local Selenium install make sure it is running first. You must also, depending on the browsers your want to test, have add-ons installed or additional Selenium drivers running

```
java -jar selenium-server-standalone-2.47.1.jar
```

And optionally:
```
./chromedriver
```

Make sure you have phantomjs installed as well if you want to test headlessly:
```
sudo npm install -g phantomjs
```

First, make sure you have a ```results```, ```logs``` folder in your project so that wdrunner can write to them.

Then can include the wdrunner in your node project by adding the following into a new JS file (eg. ```index.js```):

```
var wdrunner = require('maqe-wdrunner');
wdrunner.run({
	config: __dirname+'/wd-local.json'
}, function(){
	console.log("I'm done!");
});
```

Next, create your local config file (eg. ```wd-local.json```) based on the ```config-local-sample.json```.

Then finally run:

```
node index.js
```

---

NOTE: When you create your config JSON file make sure the tests location is written as relative to the node_modules path. So with ```maqe-wdrunner``` included in your node project and your tests are located in the root folder, your config would look like this:

```
	"tests": [
		"./../../test/**/*.js"
	],
```

This is a temporary workaround until it can be fixed more smoothly.

NOTE: You must have ```chai``` installed in your project if you include ```maqe-wdrunner```, otherwise it will hang before you get to any tests. Need to see if there's a way to use sub-module version of ```chai``` instead of installing in all the places.

---

Also, to test against Browserstack just generate a Browserstack specific JSON config file.

```
var wdrunner = require('maqe-wdrunner');
wdrunner.run({
	config: __dirname+'/app/config/wd-bs.json'
}, , function(){
	console.log("I'm done!");
});
```
