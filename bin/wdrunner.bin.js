#!/usr/bin/env node

'use strict';

var wdrunner = require('../index');
var argv = require('yargs').argv;

// arguement of config and c is a filename, in full path!!
var configFile = argv.config || argv.c;
var domain = argv.domain || argv.d || '';

if (!configFile) {
    console.log("Please specify config (option -c)");
    process.exit(1);
}

if (domain == '') {
    console.log("No domain name specified, using default domain setting from the test file");
} else {
    console.log("Domain: " + domain);
}

wdrunner.run({
    config: configFile,
    domain: domain
    // config: __dirname+'/wd-browserstack.json'
}, function(){
    console.log("Test runner completed.");
});
