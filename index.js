'use strict';

var fs = require('fs');
var extend = require('node.extend');
var opts = require('nomnom').parse();
var async = require('async');
var lodash = require('lodash');
var kommando = require('kommando');

/*

TODO:
	be able to pass config from a file or object

	update reporter
		need test runner reporter wrapper
		xunit/junit style:
			testsuites
				testsuite
					testcase
			minimum could be json with comma delineation...
				does not include closing "]" bracket currently...
*/

var _default = {
	driverOptions: {
		seleniumUrl: '',
	},
	driver: 'selenium-grid',
	capabilities: [
		// {
		// 	browserName: 'phantomjs'
		// },
		// {
		// 	browserName: 'chrome',
		// },
		// {
		// 	browserName: 'firefox',
		// }
	],
	tests: [
		// './sample-tests/**/*.js'
	],
	runner: 'mocha',
	runnerOptions: {
		timeout: 150 * 1000, //150000,
		ui: 'tdd',
		reporter: 'mocha-multi',
		// reporter: 'list',
		// reporter: 'dot',
		// reporter: 'json',
		// reporter: 'xunit',
		globals: ['results', 'kommando'],
	},
	runnerKommandoGlobals: {
	},
	// client: 'wd'
	client: 'wd-promiseChain'
	// client: 'wd-promise'
};

// // var config = extend({}, _default.local);
// //should check c if is string file path, or json object
// //and load appropriately...
// var config = _default.local;
// // console.log(opts);
// if(!!opts.c){
// 	config = JSON.parse(fs.readFileSync(opts.c, 'utf8'));
// }
// var config =  || _default.local;

// console.log(config);
// fs.stat(config, function(err, stat) {
// 	if(err !== null) {
// 		//load in config
// 		config = JSON.parse(fs.readFileSync(config, 'utf8'));
// 	}
// });

//maybe should be able to overwrite existing props in config via opts
//so an extend on the config basically...
/*
	caps
	tests
	etc...
*/
// var target = opts.t || 'local';

var run = function(obj, callback) {

	//client will pass in config file through 'config' which will overwrite the default setting
	var config = JSON.parse(fs.readFileSync(obj.config, 'utf8'));
	var domainInput = obj.domain;

	// bail out if there's error while reading the config file
	if (!config) {
		console.log('config file is blank');
		process.exit(2);
	}

	// merge setting to the default one
	config = extend({}, _default, config);

	if (domainInput) {
		config.runnerKommandoGlobals.domain = domainInput;
	}

	//setup var for mocha-multi
	//TODO:
	//	file output is overwriting previous run...
	//	need to either append OR save diff files with run ID (timestamp) and browserName
	var ts = new Date().getTime();
	if (typeof config.results !== 'undefined') {
	
		if (!fs.existsSync(config.results.path)) {
			console.log("Results Folder Not Found, exit");
			process.exit(4);
		}
		process.env['multi'] = 'list=- json='+config.results.path+'/'+config.results.fileName;
	} else {
		// process.env['multi'] = 'list=- json=./results/results-'+ts+'.json xunit=./results/results-'+ts+'.xml';
		process.env['multi'] = 'list=- json=./results/results-'+ts+'.json';
	}

	// bail out if the necessary configuration not ready
	if (typeof config.driverOptions.seleniumUrl === 'undefined' || 
		config.capabilities.length === 0 || 
		config.tests.length === 0) {
		console.log("Invalid setting, please check Selenium URL, or Capacities, or Tests");
		process.exit(3);
	}

	async.series([
		kommando.bind(null, config)
	], function(error, results) {
		var passed = lodash.every(lodash.map(results, function(result) {
			return lodash.every(result, 'passed');
		}));
		if (!passed) {
			error = new Error('One or more tests did not pass.');
		}

		// close the result file with ], for some reason mocha left the bracket opened.
		fs.appendFileSync(config.results.path+'/'+config.results.fileName, ']');

		if (error) {
			console.error(error);
			callback(results);
			process.exit(0);
		} else {
			callback(results);
			process.exit(0);
		}
	});
}

module.exports = {
	run: run
}