'use strict';

//setup vars for testing
var assert = require('chai').assert;

//the actual domain to be tested
var domain = 'http://qa.maqe.com/maqe',
	waitElement = '#main-content',
	browser = kommando.browser;

/*
//	if you need to do something before the suites run use this...
*/
// suiteSetup(function() {
// 	console.log('--------------SUITE SETUP-----------------');
// });

suite('load maqe.com', function(){

	/*
	//	if you need to do something before each test starts use this...
	*/
	// setup(function() {
	// 	console.log('------------------SETUP------------------');
	// });

	/*
	//	if you need to do something after each test finishes use this...
	*/
	// teardown(function() {
	// 	console.log('-----------------TEARDOWN-----------------');
	// });

	test('check main content loads', function(done){
		// console.log(browser);
		browser
		.get(domain+'/')
		//can use a wait for or just poll the element directly
		//if your site is loading content dynamically then waitFor is probably best
		// .waitForElementsByCss('.intro-content', 150000)
		//need to use elementsByCss if we want to get the length, otherwise will error
		.elementsByCss('.intro-content')
		.then(function(el){
			// console.log(el.length);
			return assert.equal(el.length, 1, 'check that content element exists');
		})
		.then(done, done);
	});

	test('check maqe slogan', function(done){
		browser
		.get(domain+'/')
		.elementByCss('.intro-content strong')
		.text()
		.then(function(text){
			// console.log(text);
			return assert.equal(text, "Good people, great apps.", 'check content element text');
		})
		.then(done, done);
	});

	test('check first profile in profile list', function(done){
		browser
		.get(domain+'/')
		.elementByCss('.profile-list li:first-child .nickname')
		.text()
		.then(function(text){
			// console.log(text);
			return assert.equal(text, 'Drew', 'check that Drew is first');
		})
		.then(done, done);
	});

});
