'use strict';

//setup vars for testing
const assert = require('chai').assert;

//the actual domain to be tested
const domain = kommando.domain || 'http://testsweet.maqe.com/',
	browser = kommando.browser;

suite('load maqe.com', function(){

	test('check main content loads', function(done){
		browser
		.get(domain+'/')
		//can use a wait for or just poll the element directly
		//if your site is loading content dynamically then waitFor is probably best
		// .waitForElementsByCss('.intro-content', 150000)
		//need to use elementsByCss if we want to get the length, otherwise will error
		.elementsByCss('.page-title')
		.then(function(el){
			// console.log(el.length);
			return assert.equal(el.length, 1, 'check that content element exists');
		})
		.then(done, done);
	});

	test('check slogan', function(done){
		browser
		.get(domain+'/')
		.elementByCss('.tagline')
		.text()
		.then(function(text){
			// console.log(text);
			return assert.equal(text, "The missing UI for your functional automation test suites.", 'check content element text');
		})
		.then(done, done);
	});

});
