'use strict';

//setup vars for testing
var assert = require('chai').assert;

/*
//	simple tests here, no browser and nothing really being checked...
*/
suite('simple test', function(){

	test('assert ok', function(done){
		assert.ok(true, 'true is true');
		//since we're not using the browser or doing any promise stuff can just return done here...
		done();
	});

	test('assert notOk', function(done){
		assert.notOk(false, 'false is not true');
		done();
	});

	test('do a failure', function(done){
		assert.ok(false, 'false is not true');
		done();
	});

});
